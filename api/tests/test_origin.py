#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>


def test_views_version(client):
    res = client.get('/v1/version')
    assert res.status_code == 200

    assert len(res.data) >= 0
