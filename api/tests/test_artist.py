#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

import json
from uuid import uuid4

import pytest

from smapi.artist.model import Artist

TEST_NAME = "foo"
MIMETYPE = "application/json"
HEADERS = {"Content-Type": MIMETYPE, "Accept": MIMETYPE}


def test_model_init():
    a = Artist(name=TEST_NAME)
    assert isinstance(a, Artist)
    assert a.name == TEST_NAME
    assert a.id is None
    with pytest.raises(ValueError):
        a = Artist()
    assert a.ts_created is not None
    assert isinstance(a.ts_created, float)


def test_model_repr():
    a = Artist(name=TEST_NAME)
    assert "{}".format(a).startswith("<Artist")


def test_model_to_dict():
    a = Artist(name=TEST_NAME)
    assert isinstance(a.to_dict(), dict)
    assert a.to_dict().get("name") == TEST_NAME
    assert a.to_dict().get("id") is None
    a.id = 42
    assert a.to_dict().get("id") == 42
    assert a.to_dict(no_id=True).get("id") is None


def test_model_save(os_client):
    a = Artist(name=TEST_NAME)
    a.save()


def test_model_get(os_client):
    a = Artist(name=TEST_NAME)
    a.save()
    b = Artist.get(id=a.id)
    assert b.id == a.id
    c = Artist.get(id="fake")
    assert c is None


def test_model_search(os_client):
    a = Artist(name=TEST_NAME)
    a.save()

    res = Artist.search(name="fake")
    assert isinstance(res, tuple)
    assert isinstance(res[0], list)
    assert len(res[0]) == 0
    assert isinstance(res[1], int)
    assert res[1] == 0

    res = Artist.search(name=TEST_NAME)
    assert isinstance(res, tuple)
    assert isinstance(res[0], list)
    if len(res[0]) > 0:
        assert isinstance(res[1], int)
        assert res[1] > 0
        if len(res[0]) < res[1]:
            res_next = Artist.search(name=TEST_NAME, offset=len(res[0]))
            assert isinstance(res_next, tuple)
            assert isinstance(res_next[0], list)
            assert len(res_next[0]) > 0
            assert isinstance(res_next[1], int)
            assert res_next[1] > 0


def test_model_get_by_name(os_client):
    _UNIQUE_NAME = str(uuid4())
    a = Artist(name=_UNIQUE_NAME)
    a.save()

    res = Artist.get_by_name(name="fake")
    assert res is None
    res = Artist.get_by_name(name=a.name)
    if res is not None:
        assert isinstance(res, Artist)


def test_views_get(client):
    a = Artist(name=TEST_NAME)
    a.save()
    res = client.get("/v1/artist/")
    assert res.status_code == 405

    res = client.get("/v1/artist/INEXISTANT")
    assert res.status_code == 404

    res = client.get("/v1/artist/{}".format(a.id))
    assert res.status_code == 200
    data_json = json.loads(res.data)
    assert len(data_json) >= 0
    assert data_json.get("artist") is not None


def test_views_search(client):
    a = Artist(name=TEST_NAME)
    a.save()

    data = {}
    res = client.post(
        "/v1/artist/search", data=json.dumps(data), headers=HEADERS
    )
    assert res.status_code == 422

    data = {"artist": {"name": TEST_NAME}}
    res = client.post(
        "/v1/artist/search", data=json.dumps(data), headers=HEADERS
    )
    assert res.status_code == 200
    data_json = json.loads(res.data)
    assert len(data_json) >= 0
    assert data_json.get("artists") is not None
    if (
        data_json.get("nextPageToken") is not None
        and data_json.get("nextPageToken") != ""
    ):
        data = {"page-token": data_json.get("nextPageToken")}
        res = client.post(
            "/v1/artist/search", data=json.dumps(data), headers=HEADERS
        )
        assert res.status_code == 200
        next_data_json = json.loads(res.data)
        assert len(next_data_json) >= 0
