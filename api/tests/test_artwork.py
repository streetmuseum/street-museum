#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

from smapi.artwork.model import Artwork
from smapi.artist.model import Artist


TEST_NAME = "foo"
TEST_ARTWORK = {
    "id": 42,
    "image": "https://unsplash.com/photos/IwSUpNHsEfc",
    "artist": TEST_NAME,
    "posted_by": "bar",
    "description": "nice picture!",
    "tags": ["picture"],
    "location": {"lat": 48.8534, "lon": 2.3488},
    "time": {"start": 1619603202},
    "ts_created": 1619603202,
}
TEST_ARTIST = Artist(name=TEST_NAME)


def test_model_init():
    res = Artwork()
    assert isinstance(res, Artwork)

    res = Artwork(image="foo")
    assert res.image == "foo"


def test_model_repr():
    res = Artwork()
    assert "{}".format(res).startswith("<Artwork from")


def test_model_to_dict():
    res = Artwork(**TEST_ARTWORK)
    assert res.to_dict() == TEST_ARTWORK

    WITH_ARTIST_DICT = TEST_ARTWORK.copy()
    WITH_ARTIST_DICT["artist_obj"] = TEST_ARTIST.to_dict()
    res = Artwork(**TEST_ARTWORK)
    res.artist_obj = TEST_ARTIST
    assert res.to_dict() == WITH_ARTIST_DICT

    NO_ID_DICT = TEST_ARTWORK.copy()
    NO_ID_DICT.pop("id")
    res = Artwork(**TEST_ARTWORK)
    assert res.to_dict(no_id=True) == NO_ID_DICT


def test_model_save():
    NO_ID_DICT = TEST_ARTWORK.copy()
    NO_ID_DICT.pop("id")
    a = Artwork(**NO_ID_DICT)
    a.save()
    assert a.id is not None
    a.description = "foobar"
    a.save()
    res = Artwork.get(id=a.id)
    assert res is not None
    assert res.description == a.description


def test_model_enrich():
    LOCAL_ARTIST = Artist(name=TEST_NAME)
    LOCAL_ARTIST.save()
    NO_ID_DICT = TEST_ARTWORK.copy()
    NO_ID_DICT.pop("id")
    NO_ID_DICT["artist"] = LOCAL_ARTIST.id
    a = Artwork(**NO_ID_DICT)
    a.save()

    assert hasattr(a, "artist_obj") is False
    a.enrich()
    assert hasattr(a, "artist_obj") is True
    assert a.artist_obj is not None
    assert a.artist_obj.to_dict() == LOCAL_ARTIST.to_dict()


def test_model_delete():
    NO_ID_DICT = TEST_ARTWORK.copy()
    NO_ID_DICT.pop("id")
    a = Artwork(**NO_ID_DICT)
    res = a.delete()
    assert res is None

    a.save()
    refID = a.id

    res = a.delete()
    assert res == refID
    b = Artwork.get(id=refID)
    assert b is None


def test_model_get():
    NO_ID_DICT = TEST_ARTWORK.copy()
    NO_ID_DICT.pop("id")
    a = Artwork(**NO_ID_DICT)
    a.save()

    res = Artwork.get(id="INEXISTANT_ID")
    assert res is None

    res = Artwork.get(id=a.id)
    assert res is not None


def test_model_search():
    LOCAL_ARTIST = Artist(name=TEST_NAME)
    LOCAL_ARTIST.save()
    NO_ID_DICT = TEST_ARTWORK.copy()
    NO_ID_DICT.pop("id")
    NO_ID_DICT["artist"] = LOCAL_ARTIST.id
    a = Artwork(**NO_ID_DICT)
    a.save()
    a.enrich()
    print(a.to_dict(no_child=True))

    res = Artwork.search()
    assert res is not None
    assert isinstance(res, tuple)
    # assert res[1] > 0
    assert isinstance(res[0], list)
    assert len(res[0]) > 0

    res = Artwork.search(posted_by="bar")
    assert res is not None
    assert res[1] > 0
    assert len(res[0]) > 0

    res = Artwork.search(posted_by="foo")
    assert res is not None
    assert res[1] == 0
    assert len(res[0]) == 0

    res = Artwork.search(artists=["inexistantartist"])
    assert res is not None
    assert res[1] == 0
    assert len(res[0]) == 0
