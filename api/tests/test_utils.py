#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

from uuid import UUID
from smapi import utils
import pytest

TEST_UUID = UUID('f9ee0a03-3f1c-4084-9e73-e4ece4efb974')
TEST_UUID_ENCODED = '-e4KAz8cQISec-Ts5O-5dA'
TEST_DICT = {'foo': 'foo', 'bar': 42}
TEST_DICT_ENCODED = 'eyJmb28iOiAiZm9vIiwgImJhciI6IDQyfQ'


def test_base64Encode():
    with pytest.raises(TypeError):
        res = utils.base64Encode(u=42)

    res = utils.base64Encode(TEST_UUID)
    assert res == TEST_UUID_ENCODED


def test_base64Decode():
    with pytest.raises(TypeError):
        res = utils.base64Decode(u=42)

    res = utils.base64Decode(u='42')
    assert res is None

    res = utils.base64Decode(TEST_UUID_ENCODED)
    assert res == TEST_UUID


def test_nextPageTokenEncode():
    res = utils.nextPageTokenEncode("")
    assert res is not None

    res = utils.nextPageTokenEncode(TEST_DICT)
    assert res == TEST_DICT_ENCODED


def test_nextPageTokenDecode():
    res = utils.nextPageTokenDecode("")
    assert res is None

    res = utils.nextPageTokenDecode(TEST_DICT_ENCODED)
    assert res == TEST_DICT
