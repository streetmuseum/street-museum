#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

import json
from uuid import uuid4

import pytest

from smapi.tag.model import Tag

TEST_TAG = "foo"
MIMETYPE = "application/json"
HEADERS = {"Content-Type": MIMETYPE, "Accept": MIMETYPE}


def test_model_init():
    t = Tag(tag=TEST_TAG)
    assert isinstance(t, Tag)
    assert t.tag == TEST_TAG
    with pytest.raises(ValueError):
        t = Tag()
    assert t.ts_created is not None
    assert isinstance(t.ts_created, float)


def test_model_repr():
    t = Tag(tag=TEST_TAG)
    assert "{}".format(t).startswith("<Tag")


def test_model_to_dict():
    t = Tag(tag=TEST_TAG)
    assert isinstance(t.to_dict(), dict)
    assert t.to_dict().get("tag") == TEST_TAG


def test_model_save(os_client):
    t = Tag(tag=TEST_TAG)
    t.save()


def test_model_search(os_client):
    t = Tag(tag=TEST_TAG)
    t.save()

    res = Tag.search(pattern="fake")
    assert isinstance(res, tuple)
    assert isinstance(res[0], list)
    assert len(res[0]) == 0
    assert isinstance(res[1], int)
    assert res[1] == 0

    res = Tag.search(pattern=TEST_TAG)
    assert isinstance(res, tuple)
    assert isinstance(res[0], list)
    if len(res[0]) > 0:
        assert isinstance(res[1], int)
        assert res[1] > 0
        if len(res[0]) < res[1]:
            res_next = Tag.search(pattern=TEST_TAG, offset=len(res[0]))
            assert isinstance(res_next, tuple)
            assert isinstance(res_next[0], list)
            assert len(res_next[0]) > 0
            assert isinstance(res_next[1], int)
            assert res_next[1] > 0


def test_model_get_by_tag(os_client):
    _UNIQUE_TAG = str(uuid4())
    t = Tag(tag=_UNIQUE_TAG)
    t.save()

    res = Tag.get_by_tag(tag="fake")
    assert res is None
    res = Tag.get_by_tag(tag=t.tag)
    if res is not None:
        assert isinstance(res, Tag)


def test_views_search(client):
    t = Tag(tag=TEST_TAG)
    t.save()

    data = {}
    res = client.post("/v1/tag/search", data=json.dumps(data), headers=HEADERS)
    assert res.status_code == 422

    data = {"tag": TEST_TAG}
    res = client.post("/v1/tag/search", data=json.dumps(data), headers=HEADERS)
    assert res.status_code == 200
    data_json = json.loads(res.data)
    assert len(data_json) >= 0
    assert data_json.get("tags") is not None
    if (
        data_json.get("nextPageToken") is not None
        and data_json.get("nextPageToken") != ""
    ):
        data = {"page-token": data_json.get("nextPageToken")}
        res = client.post(
            "/v1/tag/search", data=json.dumps(data), headers=HEADERS
        )
        assert res.status_code == 200
        next_data_json = json.loads(res.data)
        assert len(next_data_json) >= 0
