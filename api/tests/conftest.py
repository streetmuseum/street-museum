#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

import pytest
import os
from flask import g
from alembic.command import upgrade
from alembic.config import Config
from smapi import create_app
from smapi import db as _db
from smapi.artist.model import ARTIST_INDEX
from smapi.tag.model import TAG_INDEX
from opensearchpy import OpenSearch


ALEMBIC_CONFIG = os.path.join(
    os.path.dirname(__file__), "../migrations/alembic.ini"
)

TESTDB_PATH = "/tmp/streetmuseum.test.sqlite"

_mimetype = "application/json"
HEADERS = {"Content-Type": _mimetype, "Accept": _mimetype}


def apply_migrations():
    """Applies all alembic migrations."""
    config = Config(ALEMBIC_CONFIG)
    upgrade(config, "head")


@pytest.fixture(scope="session")
def app(request):
    app = create_app(
        {
            "TESTING": True,
            "SECRET_KEY": "dev",
            "SQLALCHEMY_DATABASE_URI": "sqlite:///{}".format(TESTDB_PATH),
            "SQLALCHEMY_TRACK_MODIFICATIONS": False,
            "OPENSEARCH_HOST": os.environ.get("OPENSEARCH_HOST", "localhost"),
            "OPENSEARCH_PORT": os.environ.get("OPENSEARCH_PORT", "9200"),
            "OPENSEARCH_INITIAL_ADMIN_PASSWORD": os.environ.get(
                "OPENSEARCH_INITIAL_ADMIN_PASSWORD", "Very_Secure_Passw0rd"
            ),
            "OPENSEARCH_DEFAULT_SIZE": 2,
        }
    )

    # Establish an application context before running the tests.
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


@pytest.fixture(scope="session")
def db(app, request):
    if os.path.exists(TESTDB_PATH):
        os.unlink(TESTDB_PATH)

    def teardown():
        _db.drop_all()
        os.unlink(TESTDB_PATH)

    _db.app = app
    with app.app_context():
        apply_migrations()

    request.addfinalizer(teardown)
    return _db


@pytest.fixture(scope="session")
def os_client(app, request):
    _os_client = OpenSearch(
        hosts=[
            {
                "host": app.config.get("OPENSEARCH_HOST"),
                "port": app.config.get("OPENSEARCH_PORT"),
            }
        ],
        verify_certs=False,
        http_auth=(
            "admin",
            app.config.get("OPENSEARCH_INITIAL_ADMIN_PASSWORD"),
        ),
    )
    if not _os_client.ping():
        app.logger.error("ES Connection failed")

    g.os_client = _os_client

    def teardown():
        _os_client.indices.delete(index=ARTIST_INDEX)
        _os_client.indices.create(index=ARTIST_INDEX)
        _os_client.indices.delete(index=TAG_INDEX)
        _os_client.indices.create(index=TAG_INDEX)

    res = _os_client.indices.exists(index=ARTIST_INDEX)
    if res:
        _os_client.indices.delete(index=ARTIST_INDEX)
    res = _os_client.indices.create(index=ARTIST_INDEX)
    res = _os_client.indices.exists(index=TAG_INDEX)
    if res:
        _os_client.indices.delete(index=TAG_INDEX)
    res = _os_client.indices.create(index=TAG_INDEX)

    request.addfinalizer(teardown)
    return _os_client


@pytest.fixture(scope="session")
def client(app):
    return app.test_client()


@pytest.fixture(scope="session")
def runner(app):
    return app.test_cli_runner()
