#!/bin/sh

PROGDIR=$(dirname "$0")
readonly PROGDIR

cd "${PROGDIR}" || exit;
flask db upgrade
flask artist init;
flask artwork init;
flask tag init;
exec gunicorn smapi:app --workers 4 --bind 0.0.0.0:5000 --timeout 1200;
