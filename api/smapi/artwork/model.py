#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

from datetime import datetime
from opensearchpy import exceptions

from flask import g, current_app

from smapi.artist.model import Artist

ARTWORK_INDEX = "artwork"
ARTWORK_MAPPING = {
    "properties": {
        "image": {"type": "keyword"},
        "artist": {"type": "keyword"},
        "posted_by": {"type": "keyword"},
        "description": {"type": "text"},
        "tags": {"type": "keyword"},
        "location": {"type": "geo_point"},
        "time": {
            "properties": {"start": {"type": "date"}, "end": {"type": "date"}}
        },
        "ts_created": {"type": "date"},
    }
}


class Artwork(object):
    def __init__(
        self,
        id=None,
        image=None,
        artist=None,
        posted_by=None,
        description=None,
        tags=None,
        location=None,
        time=None,
        ts_created=None,
    ):
        self.id = id
        self.image = image
        self.artist = artist
        self.posted_by = posted_by
        self.description = description
        self.tags = tags
        self.location = location
        self.time = time

        self.ts_created = ts_created
        if self.ts_created is None:
            self.ts_created = datetime.timestamp(datetime.utcnow())

    def __repr__(self):
        """String representation of the object"""
        return "<Artwork from {}>".format(self.artist)

    def to_dict(self, no_id=False, no_child=False):
        """Dict representation of the object

        Args:
            no_id (bool, optional): Do not include field 'id' in result.
                Defaults to False.
            no_child (bool, optional): Do no include 'artist_obj' in result.
                Defaults to False.

        Returns:
            dict: Dict representation of 'self'
        """
        a = dict(
            id=self.id,
            image=self.image,
            artist=self.artist,
            posted_by=self.posted_by,
            description=self.description,
            tags=self.tags,
            location=self.location,
            time=self.time,
            ts_created=self.ts_created,
        )
        if no_id:
            a.pop("id")

        if hasattr(self, "artist_obj") and not no_child:
            a["artist_obj"] = self.artist_obj.to_dict()

        return a

    def save(self):
        """Upsert the object in the ES, based on ID parameter"""

        if self.id is None:
            res = g.os_client.index(
                index=ARTWORK_INDEX,
                body=self.to_dict(no_id=True, no_child=True),
            )
            self.id = res["_id"]
        else:
            res = g.os_client.index(
                index=ARTWORK_INDEX,
                id=self.id,
                body=self.to_dict(no_id=True, no_child=True),
            )

    def enrich(self):
        """Enrich an Artwork object with its child objects (Artist)"""
        self.artist_obj = None

        if self.artist is not None:
            artist = Artist.get(id=self.artist)
            if artist is not None:
                self.artist_obj = artist

    def delete(self):
        """Delete a specific artwork from OpenSearch"""
        if self.id is not None:
            res = g.os_client.delete(index=ARTWORK_INDEX, id=self.id)

            if res.get("result") == "deleted":
                return res.get("_id")

        return None

    def get(id):
        """Get a specific artwork, from its ID"""
        try:
            res = g.os_client.get(index=ARTWORK_INDEX, id=id)
        except exceptions.NotFoundError:
            return None

        return Artwork(id=res.get("_id"), **res.get("_source"))

    def search(posted_by=None, tags=None, artists=None, bounds=None, offset=0):
        """Searches for artworks, and return a list of matching artworks

        Args:
            posted_by (string): Author
            tags (list of string): list of Tags
            artists (list of string): list of Artists (safe_id)
            bounds (dict of geopoint): NE / SW points of the bounds
            offset (int): Number of hits to skip (in case of pagination)

        Returns:
            (tuple(array of Artwork, int)): a tuple of Page of matching Artwork
                and the total number of hits
        """
        query = {
            "from": offset,
            "size": current_app.config.get("OPENSEARCH_DEFAULT_SIZE"),
            "sort": [{"time.start": "desc"}, "_score"],
            "query": {"bool": {"must": [], "should": [], "filter": []}},
        }

        if posted_by is not None:
            query["query"]["bool"]["must"].append(
                {"term": {"posted_by": posted_by}}
            )

        if artists is not None and len(artists) > 0:
            if len(artists) == 1:
                query["query"]["bool"]["must"].append(
                    {"term": {"artist": artists[0]}}
                )
            else:
                query["query"]["bool"]["must"].append(
                    {"terms": {"artist": artists}}
                )

        if tags is not None and len(tags) > 0:
            for tag in tags:
                query["query"]["bool"]["must"].append({"term": {"tags": tag}})

        if bounds is not None:
            query["query"]["bool"]["filter"].append(
                {"geo_bounding_box": {"location": bounds}}
            )

        current_app.logger.debug(query)
        res = g.os_client.search(index=ARTWORK_INDEX, body=query)

        resultsCount = res["hits"]["total"]["value"]

        artworks = []
        for a in res["hits"]["hits"]:
            artworks.append(Artwork(id=a.get("_id"), **a.get("_source")))

        return (artworks, resultsCount)
