#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

import click
import json
from flask import Blueprint, jsonify, current_app, request
from .model import Artwork, ARTWORK_INDEX, ARTWORK_MAPPING
from smapi.artist.model import Artist
from smapi.auth import requires_auth
from smapi.user.controller import get_current_user
from smapi.utils import upload_file, delete_file
from smapi.utils import nextPageTokenEncode, nextPageTokenDecode
from opensearchpy import OpenSearch

artwork = Blueprint("artwork", __name__, url_prefix="/v1/artwork")


@artwork.route("/<string:artwork_id>", methods=["GET"])
def get(artwork_id):
    """Get an Artwork"""
    if artwork_id is None:
        return ("missing artwork identifier", 422)

    artwork = Artwork.get(id=artwork_id)
    if artwork is None:
        return ("unknown artwork", 404)

    artwork.enrich()

    return jsonify({"artwork": artwork.to_dict()})


@artwork.route("/search", methods=["POST"])
def search():
    """Search for artworks based on various criteria"""
    offset = 0
    parsedPostedBy = None
    artists = None
    bounds = None
    tags = None

    if request.json is not None:
        pageToken = request.json.get("page-token", None)
        if pageToken is not None and pageToken != "":
            pageToken = nextPageTokenDecode(pageToken)
            if pageToken is not None:
                offset = pageToken.get("offset")
                parsedPostedBy = pageToken.get("posted_by")
                artists = pageToken.get("artists")
                bounds = pageToken.get("bounds")
                tags = pageToken.get("tags")
        else:
            if request.json.get("mine", False):
                current_user = get_current_user()
                if current_user is None:
                    return (
                        "can't find personnal artworks, should be logged",
                        403,
                    )

                parsedPostedBy = current_user.safe_id

            if (
                request.json.get("artist", None) is not None
                and request.json.get("artist") != ""
            ):

                (artistsResult, _) = Artist.search(
                    name=request.json.get("artist")
                )
                artists = [a.id for a in artistsResult]

            if request.json.get("bounds", None) is not None:
                rawbounds = request.json.get("bounds")
                bounds = {
                    "top_left": {"lat": rawbounds["n"], "lon": rawbounds["w"]},
                    "bottom_right": {
                        "lat": rawbounds["s"],
                        "lon": rawbounds["e"],
                    },
                }

            if request.json.get("tags", None) is not None:
                tags = json.loads(request.json.get("tags"))

    (artworks, artworksCount) = Artwork.search(
        posted_by=parsedPostedBy,
        tags=tags,
        artists=artists,
        bounds=bounds,
        offset=offset,
    )

    nextPageToken = ""
    pageSize = current_app.config.get("OPENSEARCH_DEFAULT_SIZE")
    if artworksCount > (pageSize + offset):
        nextPageToken = nextPageTokenEncode(
            {
                "posted_by": parsedPostedBy,
                "tags": tags,
                "artists": artists,
                "bounds": bounds,
                "offset": offset + len(artworks),
            }
        )

    for artwork in artworks:
        artwork.enrich()

    return jsonify(
        {
            "artworks": [artwork.to_dict() for artwork in artworks],
            "nextPageToken": nextPageToken,
        }
    )


@artwork.route("/", methods=["POST"])
@requires_auth
def create():
    """Create a new Artwork from a form"""
    if request.form is None:
        return ("missing data", 422)

    if "image" not in request.files:
        return ("missing file", 204)
    file = request.files["image"]
    if file.filename == "":
        return ("missing file", 204)
    image_url = upload_file(file, current_app.config.get("S3_BUCKET_ARTWORK"))

    current_user = get_current_user()
    if current_user is None:
        return ("restricted access", 403)

    try:
        parsedTags = json.loads(request.form.get("tags"))
    except json.JSONDecodeError as e:
        current_app.logger.error("unable to parse tags: {}".format(e))
        parsedTags = None

    if "time" not in request.form:
        return ("missing time", 204)
    try:
        parsedTime = json.loads(request.form.get("time"))
    except json.JSONDecodeError as e:
        current_app.logger.error("unable to parse time: {}".format(e))
        parsedTime = None
    else:
        if parsedTime.get("end") is None:
            parsedTime.pop("end")

    if "location" not in request.form:
        return ("missing location", 204)
    try:
        parsedLocation = json.loads(request.form.get("location"))
    except json.JSONDecodeError as e:
        current_app.logger.error("unable to parse location: {}".format(e))
        parsedLocation = None

    artwork = Artwork(
        image=image_url,
        artist=request.form.get("artist"),
        posted_by=current_user.safe_id,
        description=request.form.get("description"),
        tags=parsedTags,
        location=parsedLocation,
        time=parsedTime,
    )

    artwork.save()
    artwork.enrich()

    return jsonify({"artwork": artwork.to_dict()})


@artwork.route("/<string:artwork_id>", methods=["PUT"])
@requires_auth
def update(artwork_id):
    """Updates an existing Artwork from a form"""
    if request.json is None:
        return ("missing data", 422)

    artwork = request.json.get("artwork")

    if artwork_id is None:
        return ("missing artwork identifier", 422)

    dbartwork = Artwork.get(id=artwork_id)
    if dbartwork is None:
        return ("unknown artwork", 404)

    current_user = get_current_user()
    if current_user is None:
        return ("restricted access", 403)

    if dbartwork.posted_by != current_user.safe_id:
        return ("artwork can be deleted only by owners", 403)

    dbartwork.tags = json.loads(artwork.get("tags"))
    dbartwork.time = json.loads(artwork.get("time"))
    dbartwork.location = json.loads(artwork.get("location"))
    dbartwork.artist = artwork.get("artist")
    dbartwork.description = artwork.get("description")

    dbartwork.save()
    dbartwork.enrich()

    return jsonify({"artwork": dbartwork.to_dict()})


@artwork.route("/<string:artwork_id>", methods=["DELETE"])
@requires_auth
def delete(artwork_id):
    """Delete an Artwork"""
    current_user = get_current_user()
    if current_user is None:
        return ("restricted access", 403)

    if artwork_id is None:
        return ("missing artwork identifier", 422)

    dbartwork = Artwork.get(id=artwork_id)
    if artwork is None:
        return ("unknown artwork", 422)

    if dbartwork.posted_by != current_user.safe_id:
        return ("artwork can be deleted only by owners", 403)

    if dbartwork.image is not None:
        r = delete_file(
            dbartwork.image, current_app.config.get("S3_BUCKET_ARTWORK")
        )
        if r is False:
            current_app.logger.error(
                "Unable to delete {}".format(dbartwork.image)
            )

    dbartwork.delete()
    return (jsonify({}), 204)


@artwork.cli.command("init")
@click.option("-f", "--force", is_flag=True)
def init_os(force):
    """Initialize the index in OS"""
    os_client = OpenSearch(
        hosts=[
            {
                "host": current_app.config.get("OPENSEARCH_HOST"),
                "port": current_app.config.get("OPENSEARCH_PORT"),
            }
        ],
        verify_certs=False,
        http_auth=(
            "admin",
            current_app.config.get("OPENSEARCH_INITIAL_ADMIN_PASSWORD"),
        ),
    )
    if not os_client.ping():
        current_app.logger.debug("OpenSearch Connection failed")
    res = os_client.indices.exists(index=ARTWORK_INDEX)
    if not res or force:
        if res:
            os_client.indices.delete(index=ARTWORK_INDEX)
            current_app.logger.info("Index {} deleted".format(ARTWORK_INDEX))

        res = os_client.indices.create(index=ARTWORK_INDEX, body="")
        current_app.logger.info("Index {} created".format(ARTWORK_INDEX))

    res = os_client.indices.get_mapping(index=ARTWORK_INDEX)
    if (
        not res
        or not res[ARTWORK_INDEX]
        or res[ARTWORK_INDEX].get("mappings") == {}
        or force
    ):
        res = os_client.indices.put_mapping(
            index=ARTWORK_INDEX, body=ARTWORK_MAPPING
        )
        current_app.logger.info("Mappings added for {}".format(ARTWORK_INDEX))
