#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

import click
from flask import Blueprint, jsonify, current_app, request
from .model import Artist, ARTIST_INDEX, ARTIST_MAPPING, ARTIST_DEFAULTS
from smapi.auth import requires_auth
from smapi.user.controller import get_current_user
from smapi.utils import nextPageTokenEncode, nextPageTokenDecode
from opensearchpy import OpenSearch

artist = Blueprint("artist", __name__, url_prefix="/v1/artist")


@artist.route("/<string:artist_id>", methods=["GET"])
def get(artist_id):
    """Get an Artist"""
    if artist_id is None:
        return ("missing artist identifier", 422)

    artist = Artist.get(id=artist_id)
    if artist is None:
        return ("unknown artist", 404)

    return jsonify({"artist": artist.to_dict()})


@artist.route("/search", methods=["POST"])
def search():
    """Search for artists matching the given pattern"""
    offset = 0
    artist = None
    if request.json is not None:
        pageToken = request.json.get("page-token", None)
        if pageToken is not None:
            pageToken = nextPageTokenDecode(pageToken)
            if pageToken is not None:
                offset = pageToken.get("offset")
                artist = pageToken.get("artist")
        if artist is None:
            artist = request.json.get("artist", None)
            if artist is None or artist.get("name") is None:
                return ("missing artist identifier", 422)

    (artists, artistsCount) = Artist.search(
        name=artist.get("name"), offset=offset
    )

    nextPageToken = ""
    pageSize = current_app.config.get("OPENSEARCH_DEFAULT_SIZE")
    if artistsCount > (pageSize + offset):
        nextPageToken = nextPageTokenEncode(
            {"artist": artist, "offset": offset + len(artists)}
        )

    return jsonify(
        {
            "artists": [artist.to_dict() for artist in artists],
            "nextPageToken": nextPageToken,
        }
    )


@artist.route("/", methods=["POST"])
@requires_auth
def create():
    """Create a new Artist from a form"""
    if request.json is not None:
        artist = request.json.get("artist", None)
        if artist is None:
            return ("missing artist", 422)

    current_user = get_current_user()
    if current_user is None:
        return ("restricted access", 403)

    existing_artist = Artist.get_by_name(name=artist.get("name"))
    if existing_artist is not None:
        return (
            jsonify(
                error="existing artist",
                artist=existing_artist.to_dict(no_id=True),
            ),
            409,
        )

    try:
        artist = Artist(
            name=artist.get("name"), created_by=current_user.safe_id
        )
        artist.save()
    except Exception as e:
        current_app.logger.error("Can't create artist: {}".format(e))
        return ("unable to create artist", 500)

    return jsonify({"artist": artist.to_dict(no_id=True)})


@artist.cli.command("init")
@click.option("-f", "--force", is_flag=True)
def init_os(force):
    """Initialize the index in OS"""
    os_client = OpenSearch(
        hosts=[
            {
                "host": current_app.config.get("OPENSEARCH_HOST"),
                "port": current_app.config.get("OPENSEARCH_PORT"),
            }
        ],
        verify_certs=False,
        http_auth=(
            "admin",
            current_app.config.get("OPENSEARCH_INITIAL_ADMIN_PASSWORD"),
        ),
    )
    if not os_client.ping():
        current_app.logger.debug("OpenSearch Connection failed")
    res = os_client.indices.exists(index=ARTIST_INDEX)
    if not res or force:
        if res:
            os_client.indices.delete(index=ARTIST_INDEX)
            current_app.logger.info("Index {} deleted".format(ARTIST_INDEX))

        res = os_client.indices.create(index=ARTIST_INDEX, body="")
        current_app.logger.info("Index {} created".format(ARTIST_INDEX))

    res = os_client.indices.get_mapping(index=ARTIST_INDEX)
    if (
        not res
        or not res[ARTIST_INDEX]
        or res[ARTIST_INDEX].get("mappings") == {}
        or force
    ):
        res = os_client.indices.put_mapping(
            index=ARTIST_INDEX, body=ARTIST_MAPPING
        )
        current_app.logger.info("Mappings added for {}".format(ARTIST_INDEX))

    current_app.logger.info("Adding default values in {}".format(ARTIST_INDEX))
    for artist in ARTIST_DEFAULTS:
        os_client.index(index=ARTIST_INDEX, body=artist.to_dict(no_id=True))
        current_app.logger.info("  added {}".format(artist))
