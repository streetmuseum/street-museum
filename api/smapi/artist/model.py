#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

from flask import g, current_app
from datetime import datetime
from opensearchpy import exceptions

ARTIST_INDEX = "artist"
ARTIST_MAPPING = {
    "properties": {
        "name": {"type": "search_as_you_type"},
        "created_by": {"type": "keyword"},
        "ts_created": {"type": "date"},
    }
}


class Artist(object):
    def __init__(self, id=None, name=None, created_by=None, ts_created=None):
        self.id = id
        if name is None:
            raise ValueError("missing mandatory name")
        self.name = name
        self.created_by = created_by

        self.ts_created = ts_created
        if self.ts_created is None:
            self.ts_created = datetime.timestamp(datetime.utcnow())

    def __repr__(self):
        """String representation of the object"""
        return "<Artist {}>".format(self.name)

    def to_dict(self, no_id=False):
        a = dict(
            id=self.id,
            name=self.name,
            created_by=self.created_by,
            ts_created=self.ts_created,
        )
        if no_id:
            a.pop("id")
        return a

    def save(self):
        """Saves the object in the ES"""
        res = g.os_client.index(
            index=ARTIST_INDEX, body=self.to_dict(no_id=True)
        )
        if self.id is None:
            self.id = res["_id"]

    def get(id):
        """Get a specific artist, from its ID"""
        try:
            res = g.os_client.get(index=ARTIST_INDEX, id=id)
        except exceptions.NotFoundError:
            return None

        return Artist(id=res.get("_id"), **res.get("_source"))

    def search(name="", offset=0):
        """Searches for the name in the artists, and return a list of
        matching artists

        Args:
            name (string): Name to match the Artist against
            offset (int): Number of hits to skip (in case of pagination)

        Returns:
            (tuple(array of Artist, int)): a tuple of Page of matching Artists
                and the total number of hits
        """
        query = {
            "from": offset,
            "size": current_app.config.get("OPENSEARCH_DEFAULT_SIZE"),
            "query": {
                "multi_match": {
                    "query": name,
                    "type": "bool_prefix",
                    "fields": ["name", "name._2gram", "name._3gram"],
                }
            },
        }
        res = g.os_client.search(index=ARTIST_INDEX, body=query)

        resultsCount = res["hits"]["total"]["value"]

        artists = []
        for a in res["hits"]["hits"]:
            artists.append(Artist(id=a.get("_id"), **a.get("_source")))

        return (artists, resultsCount)

    def get_by_name(name):
        """Get an Artist by its name"""
        query = {
            "query": {
                "term": {"name": {"value": name, "case_insensitive": True}}
            }
        }
        res = g.os_client.search(index=ARTIST_INDEX, body=query)

        artists = []
        for a in res["hits"]["hits"]:
            artists.append(Artist(id=a.get("_id"), **a.get("_source")))

        if len(artists) != 1:
            return None

        return artists[0]


ARTIST_DEFAULTS = [
    Artist(name="skeo"),
    Artist(name="anti."),
]
