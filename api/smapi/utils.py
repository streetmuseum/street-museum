#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

import base64
import uuid
import boto3
import json
from botocore.exceptions import ClientError
from flask import current_app


def base64Encode(u):
    """Encodes a UUID to a url-safe base64 string with no padding

    Args:
        u (uuid): UUID to encode.

    Returns:
        string: The encoded url-safe base64 string
    """
    if not isinstance(u, uuid.UUID):
        raise TypeError

    return base64.urlsafe_b64encode(u.bytes).decode("utf-8").strip("=")


def base64Decode(u):
    """Decodes a url-safe base64 string in a UUID

    Args:
        u (string): URL-safe base64 string with no padding.

    Returns:
        uuid: The decoded UUID
    """
    if not isinstance(u, str):
        raise TypeError

    b = base64.urlsafe_b64decode('{}=='.format(u))
    try:
        u = uuid.UUID(bytes=b)
    except ValueError:
        return None

    return u


def nextPageTokenEncode(d):
    """Encodes a dict into a url-safe base64 string with no padding

    Args:
        d (dict): Content to encode

    Returns:
        (string): the encoded nextPageToken
    """
    return base64.urlsafe_b64encode(
        json.dumps(d).encode('utf-8')
    ).decode("utf-8").strip("=")


def nextPageTokenDecode(t):
    """Decodes a url-safe base64 string with no padding into a dict

    Args:
        t (string): Content to decode

    Returns:
        (dict): a decoded nextPageToken
    """
    if t == "":
        return None

    b = base64.urlsafe_b64decode('{}=='.format(t))
    return json.loads(b)


def upload_file(myfile, bucket):
    """Uploads a file to an S3 bucket

    Args:
        myfile (file): File object to upload
        bucket (string): Name of the Bucket to upload to

    Returns:
        string: Public URL of the uploaded file (empty string if file not
            uploaded)
    """
    filename = '{}.jpg'.format(base64Encode(uuid.uuid4()))

    session = boto3.session.Session()
    s3_client = session.client(
        service_name='s3',
        region_name=current_app.config.get('S3_REGION'),
        use_ssl=True,
        endpoint_url=current_app.config.get('S3_ENDPOINT_URL'),
        aws_access_key_id=current_app.config.get('S3_ACCESS_KEY'),
        aws_secret_access_key=current_app.config.get('S3_SECRET_KEY')
    )
    fields = {
        'ACL': 'public-read',
        'CacheControl': 'nocache',
        'ContentType': 'image/jpeg'
    }

    try:
        _ = s3_client.upload_fileobj(
            myfile, bucket, filename, ExtraArgs=fields)
    except ClientError as e:
        current_app.logger.error('Problem in S3 upload: {}'.format(e))

    return '{}/{}/{}'.format(
        current_app.config.get('S3_ENDPOINT_URL'),
        bucket,
        filename)


def delete_file(file_url, bucket):
    """Removes a file from S3 bucket

    Args:
        file_url (string): Full URL of the file to delete
        bucket (string): Name of the Bucket to upload to

    Returns:
        boolean: The result of the deletion
    """
    filename = file_url.split('/')[-1]

    session = boto3.session.Session()
    s3_client = session.client(
        service_name='s3',
        region_name=current_app.config.get('S3_REGION'),
        use_ssl=True,
        endpoint_url=current_app.config.get('S3_ENDPOINT_URL'),
        aws_access_key_id=current_app.config.get('S3_ACCESS_KEY'),
        aws_secret_access_key=current_app.config.get('S3_SECRET_KEY')
    )

    try:
        _ = s3_client.delete_object(Bucket=bucket, Key=filename)
    except ClientError as e:
        current_app.logger.error(
            'Problem while deleting file from S3: {}'.format(e))

    else:
        return True

    return False
