#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

import os


class config(object):
    # Flask configuration
    SECRET_KEY = os.environ.get("SECRET_KEY", default="dev")

    # Database configuration
    DATA_PATH = os.environ.get(
        "DATA_PATH", default=os.path.join(os.path.dirname(__file__), "../data")
    )
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        "DATABASE_URL"
    ) or "sqlite:///" + os.path.join(DATA_PATH, "streetmuseum.sqlite")
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # OpenSearch configuration
    OPENSEARCH_HOST = os.environ.get("OPENSEARCH_HOST", default="localhost")
    OPENSEARCH_PORT = os.environ.get("OPENSEARCH_PORT", default="9200")
    OPENSEARCH_DEFAULT_SIZE = os.environ.get(
        "OPENSEARCH_DEFAULT_SIZE", default=100
    )
    OPENSEARCH_INITIAL_ADMIN_PASSWORD = os.environ.get(
        "OPENSEARCH_INITIAL_ADMIN_PASSWORD", default="Very_Secure_Passw0rd"
    )

    # Auth0 configuration
    AUTH0_DOMAIN = os.environ.get("AUTH0_DOMAIN")
    AUTH0_AUDIENCE = os.environ.get("AUTH0_AUDIENCE")
    AUTH0_CLIENT_ID = os.environ.get("AUTH0_CLIENT_ID")
    AUTH0_CLIENT_SECRET = os.environ.get("AUTH0_CLIENT_SECRET")

    # Upload configuration
    UPLOAD_FOLDER = os.environ.get("UPLOAD_FOLDER", "/tmp")
    # S3 configuration
    S3_REGION = os.environ.get("S3_REGION")
    S3_ENDPOINT_URL = os.environ.get("S3_ENDPOINT_URL")
    S3_ACCESS_KEY = os.environ.get("S3_ACCESS_KEY")
    S3_SECRET_KEY = os.environ.get("S3_SECRET_KEY")
    S3_BUCKET_ARTWORK = os.environ.get("S3_BUCKET_ARTWORK")
