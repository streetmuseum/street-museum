#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

from flask import Blueprint
from smapi.version import __version__

origin = Blueprint('origin', __name__, url_prefix='/v1')


@origin.route('/version', methods=['GET'])
def version():
    """Return the version of the API
    """
    return __version__
