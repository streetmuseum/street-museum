#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

import click
from flask import Blueprint, jsonify, request, current_app
from .model import Tag, TAG_INDEX, TAG_MAPPING, TAG_DEFAULTS
from smapi.auth import requires_auth
from smapi.user.controller import get_current_user
from smapi.utils import nextPageTokenEncode, nextPageTokenDecode
from opensearchpy import OpenSearch

tag = Blueprint("tag", __name__, url_prefix="/v1/tag")


@tag.route("/search", methods=["POST"])
def search():
    """Search for tags matching the given pattern"""
    offset = 0
    tag = None
    if request.json is not None:
        pageToken = request.json.get("page-token", None)
        if pageToken is not None:
            pageToken = nextPageTokenDecode(pageToken)
            if pageToken is not None:
                offset = pageToken.get("offset")
                tag = pageToken.get("tag")
        if tag is None:
            tag = request.json.get("tag", None)

    if tag is None:
        return ("missing tag identifier", 422)

    (tags, tagsCount) = Tag.search(pattern=tag, offset=offset)

    nextPageToken = ""
    pageSize = current_app.config.get("OPENSEARCH_DEFAULT_SIZE")
    if tagsCount > (pageSize + offset):
        nextPageToken = nextPageTokenEncode(
            {"tag": tag, "offset": offset + len(tags)}
        )

    return jsonify(
        {
            "tags": [tag.to_dict() for tag in tags],
            "nextPageToken": nextPageToken,
        }
    )


@tag.route("/", methods=["POST"])
@requires_auth
def create():
    """Create a new Tag from a form"""
    if request.json is not None:
        tag = request.json.get("tag", None)

    current_user = get_current_user()
    if current_user is None:
        return ("restricted access", 403)

    existing_tag = Tag.get_by_tag(tag=tag)
    if existing_tag is not None:
        return (jsonify(error="existing tag", tag=existing_tag.to_dict()), 409)

    dbtag = Tag(tag=tag, created_by=current_user.safe_id)
    dbtag.save()

    return jsonify({"tag": dbtag.to_dict()})


@tag.cli.command("init")
@click.option("-f", "--force", is_flag=True)
def init_os(force):
    """Initialize the index in OS"""
    os_client = OpenSearch(
        hosts=[
            {
                "host": current_app.config.get("OPENSEARCH_HOST"),
                "port": current_app.config.get("OPENSEARCH_PORT"),
            }
        ],
        verify_certs=False,
        http_auth=(
            "admin",
            current_app.config.get("OPENSEARCH_INITIAL_ADMIN_PASSWORD"),
        ),
    )
    if not os_client.ping():
        current_app.logger.debug("ES Connection failed")
    res = os_client.indices.exists(index=TAG_INDEX)
    if not res or force:
        if res:
            os_client.indices.delete(index=TAG_INDEX)
            current_app.logger.info("Index {} deleted".format(TAG_INDEX))

        res = os_client.indices.create(index=TAG_INDEX, body="")
        current_app.logger.info("Index {} created".format(TAG_INDEX))

    res = os_client.indices.get_mapping(index=TAG_INDEX)
    if (
        not res
        or not res[TAG_INDEX]
        or res[TAG_INDEX].get("mappings") == {}
        or force
    ):
        res = os_client.indices.put_mapping(index=TAG_INDEX, body=TAG_MAPPING)
        current_app.logger.info("Mappings added for {}".format(TAG_INDEX))

    current_app.logger.info("Adding default values in {}".format(TAG_INDEX))
    for tag in TAG_DEFAULTS:
        os_client.index(index=TAG_INDEX, body=tag.to_dict())
        current_app.logger.info("  added {}".format(tag))
