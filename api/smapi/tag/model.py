#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

from flask import g, current_app
from datetime import datetime

TAG_INDEX = 'tag'
TAG_MAPPING = {
    "properties": {
        "tag": {"type": "search_as_you_type"},
        "created_by": {"type": "keyword"},
        "ts_created": {"type": "date"}
    }
}


class Tag(object):
    """Simple tag attached to an Artwork
    This class is set to ease the search-as-you-type feature when playing
    with tags
    """

    def __init__(self,
                 tag=None,
                 created_by=None,
                 ts_created=None):
        if tag is None:
            raise ValueError('missing mandatory tag')
        self.tag = tag
        self.created_by = created_by
        self.ts_created = ts_created
        if ts_created is None:
            self.ts_created = datetime.timestamp(datetime.utcnow())

    def __repr__(self):
        """String representation of the object
        """
        return '<Tag {}>'.format(self.tag)

    def to_dict(self):
        return dict(
            tag=self.tag,
            created_by=self.created_by,
            ts_created=self.ts_created
        )

    def save(self):
        """Saves the object in the ES
        """
        res = g.os_client.index(index=TAG_INDEX, body=self.to_dict())
        self.id = res['_id']

    def search(pattern='', offset=0):
        """Searches for the pattern in the tags, and return a list of
        matching tags

        Args:
            pattern (string): Pattern to match the Tag against
            offset (int): Number of hits to skip (in case of pagination)

        Returns:
            (tuple(array of Tag, int)): a tuple of Page of matching Tags and
                the total number of hits
        """
        query = {
            "from": offset,
            "size": current_app.config.get('OPENSEARCH_DEFAULT_SIZE'),
            "query": {
                "multi_match": {
                    "query": pattern,
                    "type": "bool_prefix",
                    "fields": [
                        "tag",
                        "tag._2gram",
                        "tag._3gram"
                    ]
                }
            }
        }
        res = g.os_client.search(index=TAG_INDEX, body=query)

        resultsCount = res['hits']['total']['value']

        return (
            [Tag(**tag.get('_source')) for tag in res['hits']['hits']],
            resultsCount
        )

    def get_by_tag(tag):
        """Get a Tag based on its tag
        """
        query = {
            "query": {
                "term": {
                    "tag": {
                        "value": tag,
                        "case_insensitive": True
                    }
                }
            }
        }
        res = g.os_client.search(index=TAG_INDEX, body=query)

        tags = []
        for a in res['hits']['hits']:
            tags.append(
                Tag(**a.get('_source'))
            )

        if len(tags) != 1:
            return None

        return tags[0]


TAG_DEFAULTS = [
    Tag(tag='calligraphy'),
    Tag(tag='chrome'),
    Tag(tag='fatcap'),
    Tag(tag='flop'),
    Tag(tag='handstyle'),
    Tag(tag='ink'),
    Tag(tag='oldschool'),
    Tag(tag='périf'),
    Tag(tag='subway'),
    Tag(tag='tag'),
    Tag(tag='throwup'),
    Tag(tag='vandal'),
    Tag(tag='vandalism'),
]
