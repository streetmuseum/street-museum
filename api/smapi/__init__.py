#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

import os
from flask import Flask, g
from flask_cors import CORS
from dotenv import load_dotenv
from .config import config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from opensearchpy import OpenSearch


load_dotenv()
db = SQLAlchemy()
migrate = Migrate()


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_object(config)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Configure CORS
    CORS(app=app, supports_credentials=True)

    with app.app_context():
        # Configure database client
        db.init_app(app)
        migrate.init_app(app, db, render_as_batch=True)

        # Configure ES client
        @app.before_request
        def before_request():
            g.os_client = OpenSearch(
                hosts=[
                    {
                        "host": app.config.get("OPENSEARCH_HOST"),
                        "port": app.config.get("OPENSEARCH_PORT"),
                    }
                ],
                verify_certs=False,
                http_auth=(
                    "admin",
                    app.config.get("OPENSEARCH_INITIAL_ADMIN_PASSWORD"),
                ),
            )
            if not g.os_client.ping():
                app.logger.debug("OpenSearch Connection failed")

        # register blueprints
        from .origin.views import origin

        app.register_blueprint(origin)
        from .user.views import user

        app.register_blueprint(user)
        from .tag.views import tag

        app.register_blueprint(tag)
        from .artist.views import artist

        app.register_blueprint(artist)
        from .artwork.views import artwork

        app.register_blueprint(artwork)

        return app


app = create_app()
