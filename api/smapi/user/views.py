#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

from flask import Blueprint, jsonify, request, current_app
from smapi.auth import requires_auth
from .model import User
from smapi import db
from smapi.authapi import AuthApi
from smapi.user.controller import get_current_user
from datetime import datetime
import json

user = Blueprint('user', __name__, url_prefix='/v1/user')


@user.route('/me', methods=['POST'])
def register():
    """Upsert authentified user as an API user
    """
    return_user = {}
    user = None
    isNew = False

    if request.json is not None:
        user = json.loads(request.json.get('user', None))

    if user:
        if user.get('sub') is not None:
            dbuser = User().get_by_auth_id(auth_id=user.get('sub'))
        elif user.get('uuid') is not None:
            dbuser = User().get_by_uuid(safe_id=user.get('uuid'))

        if dbuser is None:
            isNew = True
            dbuser = User(
                name=user.get('nickname', user.get('name')),
                email=user.get('email'),
                email_verified=user.get('email_verified'),
                icon=user.get('picture'),
                auth_id=user.get('sub')
            )
            db.session.add(dbuser)
        else:
            if dbuser.email_verified != user.get('email_verified'):
                dbuser.email_verified = user.get('email_verified')
            if dbuser.email != user.get('email'):
                dbuser.email = user.get('email')
            dbuser.ts_updated = datetime.utcnow()

        db.session.commit()

        return_user = dbuser.to_dict()

    return jsonify({'user': return_user, 'new': isNew})


@user.route('/<string:safe_id>', methods=['PUT'])
@requires_auth
def update(safe_id):
    """Update an API user
    """
    if request.json is not None:
        user = request.json.get('user', None)

    if safe_id is None:
        return ('missing user identifier', 422)

    dbuser = User().get_by_uuid(safe_id=safe_id)
    if dbuser is None:
        return ('unknown user', 404)

    current_user = get_current_user()
    if current_user is None:
        return ('restricted access', 403)

    if (dbuser.safe_id != current_user.safe_id
            or current_user.safe_id != safe_id):
        return ('user can be updated only themselves', 403)

    if dbuser.name != user.get('name'):
        dbuser.name = user.get('name')

    if dbuser.icon != user.get('picture', user.get('icon')):
        dbuser.icon = user.get('picture', user.get('icon'))
    dbuser.ts_updated = datetime.utcnow()

    db.session.commit()

    return jsonify({'user': dbuser.to_dict()})


@user.route('/<string:encodedUUID>', methods=['DELETE'])
@requires_auth
def delete(encodedUUID):
    """Delete current API user
    """
    if encodedUUID is not None:
        dbuser = User().get_by_uuid(safe_id=encodedUUID)
        if dbuser is None:
            return('unknown user', 404)

        auth0 = AuthApi(
            domain=current_app.config.get('AUTH0_DOMAIN'),
            client_id=current_app.config.get('AUTH0_CLIENT_ID'),
            client_secret=current_app.config.get('AUTH0_CLIENT_SECRET'),
            audience=current_app.config.get('AUTH0_AUDIENCE')
        )

        current_app.logger.debug('Deleting user {} on Auth0 ({})'.format(
            dbuser, dbuser.auth_id))
        auth0.delete_user(dbuser.auth_id)

        db.session.delete(dbuser)
        db.session.commit()

    return ('', 204)
