#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <chilladx@pm.me>

from smapi import db
from smapi.utils import base64Encode, base64Decode
from datetime import datetime
import uuid


class User(db.Model):
    """User is, well, a User
    """
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(36), index=True)
    name = db.Column(db.Text)
    email = db.Column(db.Text, unique=True)
    email_verified = db.Column(db.Boolean, default=False)
    icon = db.Column(db.Text)
    auth_id = db.Column(db.Text, index=True)
    ts_created = db.Column(db.DateTime, default=datetime.utcnow)
    ts_updated = db.Column(db.DateTime)

    def __init__(self, id=None, name=None, email=None, email_verified=False,
                 icon=None, auth_id=None):
        """Init the User object
        """
        self.id = id
        self.uuid = str(uuid.uuid4())
        self.name = name
        self.email = email
        self.email_verified = email_verified
        self.icon = icon
        self.auth_id = auth_id

    def __repr__(self):
        """String representation of the object
        """
        return '<User {}>'.format(self.email)

    def to_dict(self):
        if self.ts_updated is not None:
            ts_updated = self.ts_updated.timestamp()
        else:
            ts_updated = None

        return dict(
            uuid=self.safe_id,
            name=self.name,
            email=self.email,
            email_verified=self.email_verified,
            icon=self.icon,
            ts_created=self.ts_created.timestamp(),
            ts_updated=ts_updated
        )

    def to_public_dict(self):
        return dict(
            uuid=self.safe_id,
            name=self.name,
            icon=self.icon
        )

    @property
    def safe_id(self):
        return base64Encode(u=self.uuid_object)

    @property
    def uuid_object(self):
        return uuid.UUID(self.uuid)

    def get_by_auth_id(self, auth_id):
        """Returns a User filtered on auth_id
        """
        return self.query.filter_by(
            auth_id=auth_id).first()

    def get_by_uuid(self, safe_id):
        """Returns a User filtered on auth_id
        """
        user_uuid = base64Decode(u=safe_id)
        return self.query.filter_by(
            uuid=str(user_uuid)).first()

    def get_all(self):
        """Returns all the Users
        """
        return self.query.all()
