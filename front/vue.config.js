module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "~bulma/bulma";'
      }
    }
  },
  productionSourceMap: false,
  chainWebpack: config => {
    if (process.env.NODE_ENV === 'development') {
      config.devtool('eval-source-map');
    }
  }
};
