export const EMPTY_ARTWORK = {
  artist: '',
  description: '',
  image: '',
  tags: [],
  location: {
    lat: 0,
    lon: 0
  },
  time: {
    start: new Date(0),
    end: new Date(0)
  }
};
