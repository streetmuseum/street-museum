import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faCheckCircle,
  faGlobeAfrica,
  faHashtag,
  faHighlighter,
  faIdCard,
  faImage,
  faPlus,
  faSearch,
  faSignOutAlt,
  faSpinner,
  faTrashAlt,
  faUpload,
  faUserEdit,
  faUser,
  faUserTimes
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Vue from 'vue';
import VModal from 'vue-js-modal';

import { domain, clientId, audience } from '../auth_config.json';

import App from './App.vue';
import i18n from './i18n';
import { Auth0Plugin } from './plugins/auth';
import StreetMuseumAPI from './plugins/sm-api';
import router from './router';
import store from './store';

Vue.config.productionTip = false;
// Install the authentication plugin here
Vue.use(Auth0Plugin, {
  domain,
  clientId,
  audience,
  onRedirectCallback: appState => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    );
  }
});

// configure FontAwesome icons
library.add(
  faCheckCircle,
  faGlobeAfrica,
  faHashtag,
  faHighlighter,
  faIdCard,
  faImage,
  faPlus,
  faSearch,
  faSignOutAlt,
  faSpinner,
  faTrashAlt,
  faUpload,
  faUserEdit,
  faUser,
  faUserTimes
);
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.use(VModal);
Vue.use(StreetMuseumAPI);

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app');
