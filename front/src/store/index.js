import Vue from 'vue';
import Vuex from 'vuex';

import map from './map';
import user from './user';

Vue.use(Vuex);

export default new Vuex.Store({ modules: { user, map } });
