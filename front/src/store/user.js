import StreetMuseumAPI from '@/plugins/sm-api';

export const Mutations = {
  USER_RECEIVED: 'UserReceived'
};

export const Actions = {
  UPDATE_USER: 'updateUser',
  REGISTER_USER: 'registerUser',
  DELETE_USER: 'deleteUser'
};

export default {
  state: () => ({
    user: null
  }),
  mutations: {
    UserReceived(state, payload) {
      state.user = payload;
    }
  },
  actions: {
    async registerUser({ commit }, args) {
      const retour = await StreetMuseumAPI.registerUser({
        user: args
      });
      commit(Mutations.USER_RECEIVED, retour.user);
    },
    async updateUser({ commit }, args) {
      const retour = await StreetMuseumAPI.updateUser({
        user: args
      });
      commit(Mutations.USER_RECEIVED, retour.user);
    },
    async deleteUser({ commit }, args) {
      await StreetMuseumAPI.deleteUser({
        uuid: args.uuid
      });
      commit(Mutations.USER_RECEIVED, null);
    }
  },
  getters: {
    me: state => state.user
  }
};
