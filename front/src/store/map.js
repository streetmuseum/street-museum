import StreetMuseumAPI from '@/plugins/sm-api';

export const Mutations = {
  BOUNDS_SET: 'BoundsSet',
  ARTWORKS_RECEIVED: 'ArtworksReceived',
  ARTWORKS_APPENDED: 'ArtworksAppended'
};

export const Actions = {
  SET_BOUNDS: 'setBounds',
  SEARCH_ARTWORKS: 'searchArtworks',
  APPEND_ARTWORKS: 'appendArtworks'
};

export default {
  state: () => ({
    bounds: null,
    artworks: [],
    pageToken: ''
  }),
  mutations: {
    BoundsSet(state, payload) {
      state.bounds = payload;
    },
    ArtworksReceived(state, payload) {
      state.artworks = payload.artworks;
      state.pageToken = payload.nextPageToken;
    },
    ArtworksAppended(state, payload) {
      state.artworks.push(...payload.artworks);
      state.pageToken = payload.nextPageToken;
    }
  },
  actions: {
    async setBounds({ commit }, { n, e, s, w }) {
      commit(Mutations.BOUNDS_SET, { n, e, s, w });
    },
    async searchArtworks({ commit, state }, { artist, tags, pageToken }) {
      const artworksResult = await StreetMuseumAPI.searchArtworks({
        artist,
        tags,
        bounds: state.bounds,
        pageToken: pageToken
      });
      commit(Mutations.ARTWORKS_RECEIVED, artworksResult);
    },
    async appendArtworks({ commit }, { pageToken }) {
      const artworksResult = await StreetMuseumAPI.searchArtworks({
        pageToken: pageToken
      });
      commit(Mutations.ARTWORKS_APPENDED, artworksResult);
    }
  },
  getters: {
    artworkCount: state => state.artworks.length
  }
};
