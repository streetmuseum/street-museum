import axios from 'axios';

/** @module StreetMuseumAPI */

// Init Axios BaseURL
axios.defaults.baseURL = '{{.Env.STREETMUSEUM_API}}';
if (process.env.NODE_ENV === 'development') {
  axios.defaults.baseURL = 'http://localhost:5000/v1';
}

// Axios resolves the promise for all HTTP status codes. Calling code must
// handle the HTTP status code and throw an error if needed.
axios.defaults.validateStatus = null;
axios.defaults.withCredentials = true;
axios.defaults.headers.post['Content-Type'] = 'application/json';

/**
 * `StreetMuseumAPIError` encapsulates an error during a call to the StreetMuseum API.
 *
 * @property {object} data Response object from a failed API call. May be `null` or `undefined`.
 * @augments {Error}
 */
class StreetMuseumAPIError extends Error {
  /**
   * Creates an instance of StreetMuseumAPIError.
   *
   * @param {string} message The error message.
   * @param {object} data The response object from the failed API call, if any.
   * @memberof StreetMuseumAPIError
   */
  constructor(message, data) {
    super(message);
    this.data = data;
  }
}

export { StreetMuseumAPIError };

/**
 * `StreetMuseumAPI` provides functions to interface with the StreetMuseum API.
 */
const StreetMuseumAPI = {
  /**
   * Sets the authorization token for API requests. The token will be passed in
   * all requests, using the Bearer strategy in the `Authorization` header.
   *
   * @param {string} token
   */
  setAuthorizationToken(token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  },
  /**
   * Retrieves server info, such as version.
   *
   * @returns {object} The server's info
   */
  async version() {
    const res = await axios.get('/version');
    if (res.status !== 200) {
      throw new StreetMuseumAPIError(
        'Failed to retrieve server info',
        res.data
      );
    }

    return res.data;
  },
  /**
   * Registers the current user in the API.
   *
   * @returns {Object} A JSON object with a User (key: `user`) and a flag if new (key: `new`).
   */
  async registerUser({ user }) {
    const res = await axios.post('/user/me', {
      user: user
    });
    if (res.status !== 200) {
      throw new StreetMuseumAPIError(
        'Failed to register the current user',
        res.data
      );
    }

    return res.data;
  },
  /**
   * Updates a user in the API.
   *
   * @returns {Object} A JSON object with a User (key: `user`) and a flag if new (key: `new`).
   */
  async updateUser({ user }) {
    const res = await axios.put(`/user/${user.uuid}`, {
      user: user
    });
    if (res.status !== 200) {
      throw new StreetMuseumAPIError('Failed to update the user', res.data);
    }

    return res.data;
  },
  /**
   * Deletes the current user from the API.
   *
   * @returns {Object} An empty object.
   */
  async deleteUser({ uuid }) {
    const res = await axios.delete(`/user/${uuid}`);
    if (res.status !== 204) {
      throw new StreetMuseumAPIError(
        'Failed to delete the current user',
        res.data
      );
    }

    return {};
  },
  /**
   * Searches for Artworks with several criteria.
   *
   * @param {bool} mine Search for my Artworks
   * @returns {Array} A list of Artworks
   */
  async searchArtworks({ artist, tags, bounds, mine, pageToken }) {
    const searchArgs = {};
    if (mine !== undefined) {
      searchArgs['mine'] = mine;
    }
    if (artist !== undefined) {
      searchArgs['artist'] = artist;
    }
    if (tags !== undefined) {
      searchArgs['tags'] = tags;
    }
    if (bounds !== undefined) {
      searchArgs['bounds'] = bounds;
    }
    const res = await axios.post('/artwork/search', {
      ...searchArgs,
      'page-token': pageToken
    });
    if (res.status !== 200 && res.status !== 404) {
      throw new StreetMuseumAPIError('Failed to find artworks', res.data);
    }
    return res.data;
  },
  /**
   * Get an Artwork from its ID.
   *
   * @param {string} id The Artwork ID
   * @returns {Object} The Artwork (or an empty object)
   */
  async getArtwork({ id }) {
    const res = await axios.get(`/artwork/${id}`);
    if (res.status !== 200 && res.status !== 404) {
      throw new StreetMuseumAPIError('Failed to find artwork', res.data);
    }
    return res.data.artwork;
  },
  /**
   * Create a new Artwork.
   *
   * @returns {Object} A JSON object with an Artwork (key: `artwork`).
   */
  async createArtwork({ artwork }) {
    const formData = new FormData();

    for (const key in artwork) {
      formData.append(key, artwork[key]);
    }
    const res = await axios.post('/artwork/', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
    if (res.status !== 200) {
      throw new StreetMuseumAPIError(
        'Failed to create a new artwork',
        res.data
      );
    }

    return res.data;
  },
  /**
   * Update an existing Artwork.
   *
   * @returns {Object} A JSON object with an Artwork (key: `artwork`).
   */
  async updateArtwork({ artwork }) {
    const res = await axios.put(`/artwork/${artwork.id}`, { artwork });
    if (res.status !== 200) {
      throw new StreetMuseumAPIError('Failed to update the artwork', res.data);
    }

    return res.data.artwork;
  },
  /**
   * Deletes an Artwork from the API.
   *
   * @returns {Object} An empty object.
   */
  async deleteArtwork({ artwork }) {
    const res = await axios.delete(`/artwork/${artwork.id}`);
    if (res.status !== 204) {
      throw new StreetMuseumAPIError('Failed to delete the artwork', res.data);
    }

    return {};
  },
  /**
   * Searches for Tag for a given pattern.
   *
   * @param {string} pattern The pattern to match Tags against
   * @returns {Object} An object containing the list of Tags and an optional nextPageToken in case of pagination
   */
  async searchTags({ pattern, pageToken }) {
    const res = await axios.post('/tag/search', {
      tag: pattern,
      'page-token': pageToken
    });
    if (res.status !== 200 && res.status !== 404) {
      throw new StreetMuseumAPIError('Failed to retrieve tags', res.data);
    }
    return res.data;
  },
  /**
   * Creates a Tag.
   *
   * @param {string} tag The Tag to create
   * @returns {Object} The created Tag
   */
  async createTag({ tag }) {
    const res = await axios.post('/tag/', { tag });
    if (res.status !== 200 && res.status !== 409) {
      throw new StreetMuseumAPIError('Failed to create tag', res.data);
    }
    return res.data.tag;
  },
  /**
   * Searches for Artist for a given pattern.
   *
   * @param {string} pattern The pattern to match Artists against
   * @returns {Object} An object containing the list of Artists and an optional nextPageToken in case of pagination
   */
  async searchArtists({ pattern, pageToken }) {
    const res = await axios.post('/artist/search', {
      artist: {
        name: pattern
      },
      'page-token': pageToken
    });
    if (res.status !== 200 && res.status !== 404) {
      throw new StreetMuseumAPIError('Failed to retrieve artists', res.data);
    }
    return res.data;
  },
  /**
   * Creates an Artist.
   *
   * @param {string} name The name of the Artist to create
   * @returns {Object} The created Artist
   */
  async createArtist({ name }) {
    const res = await axios.post('/artist/', {
      artist: {
        name: name
      }
    });
    if (res.status !== 200 && res.status !== 409) {
      throw new StreetMuseumAPIError('Failed to create artist', res.data);
    }
    return res.data.artist;
  },
  /**
   * Get an Artist from its ID.
   *
   * @param {string} id The Artist ID
   * @returns {Object} The Artist (or an empty object)
   */
  async getArtist({ id }) {
    const res = await axios.get(`/artist/${id}`);
    if (res.status !== 200 && res.status !== 404) {
      throw new StreetMuseumAPIError('Failed to find artist', res.data);
    }
    return res.data.artist;
  },
  /**
   * Vue API plugin loader function.
   *
   * @param {object} Vue The Vue instance.
   */
  install(Vue) {
    if (Vue.prototype.$streetmuseum) {
      throw new Error('$streetmuseum already exists on Vue prototype');
    }

    Vue.prototype.$streetmuseum = this;
  }
};

export default StreetMuseumAPI;
