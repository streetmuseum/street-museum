import Vue from 'vue';
import Router from 'vue-router';

import { authGuard } from '@/plugins/auth/authGuard';

import * as Routes from './routes';

Vue.use(Router);

const routes = [
  {
    path: '/',
    name: Routes.MUSEUM,
    component: () =>
      import(/* webpackChunkName: "museum" */ '@/views/Museum.vue')
  },
  {
    path: '/latest',
    name: Routes.LATEST,
    props: { latest: true },
    component: () =>
      import(/* webpackChunkName: "artworkslist" */ '@/views/ArtworksList.vue')
  },
  {
    path: '/artist/:id',
    name: Routes.ARTIST,
    props: true,
    component: () =>
      import(/* webpackChunkName: "artist" */ '@/views/Artist.vue')
  },
  {
    path: '/about',
    name: Routes.ABOUT,
    component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
  },
  {
    path: '/profile',
    name: Routes.PROFILE,
    component: () =>
      import(/* webpackChunkName: "profile" */ '@/views/Profile.vue'),
    beforeEnter: authGuard
  },
  {
    path: '/user/edit/:id',
    name: Routes.USER_EDIT,
    props: true,
    component: () =>
      import(/* webpackChunkName: "useredit" */ '@/views/UserEdit.vue'),
    beforeEnter: authGuard
  },
  {
    path: '/artwork/new',
    name: Routes.ARTWORK_CREATE,
    component: () =>
      import(
        /* webpackChunkName: "artworkcreate" */ '@/views/ArtworkCreate.vue'
      ),
    beforeEnter: authGuard
  },
  {
    path: '/artwork/edit/:id',
    name: Routes.ARTWORK_EDIT,
    props: true,
    component: () =>
      import(/* webpackChunkName: "artworkedit" */ '@/views/ArtworkEdit.vue'),
    beforeEnter: authGuard
  },
  {
    path: '/tag/:tag',
    name: Routes.TAG,
    props: true,
    component: () =>
      import(/* webpackChunkName: "artworkslist" */ '@/views/ArtworksList.vue')
  }
];

const router = new Router({
  mode: 'history',
  routes: routes
});

router.routes = Routes;

export default router;

export { Routes };
