import Vue from 'vue';
import VueI18n from 'vue-i18n';

import getBrowserLocale from '@/i18n/get-browser-locale';
import {
  supportedLocalesInclude,
  defaultLocale
} from '@/i18n/supported-locales';

Vue.use(VueI18n);

function loadLocaleMessages() {
  const locales = require.context('.', true, /[A-Za-z0-9-_,\s]+\.json$/i);
  const messages = {};
  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i);
    if (matched && matched.length > 1) {
      const locale = matched[1];
      messages[locale] = locales(key);
    }
  });
  return messages;
}

function getStartingLocale() {
  const browserLocale = getBrowserLocale({ countryCodeOnly: true });

  if (supportedLocalesInclude(browserLocale)) {
    return browserLocale;
  } else {
    return defaultLocale;
  }
}

export default new VueI18n({
  locale: getStartingLocale(),
  fallbackLocale: defaultLocale,
  messages: loadLocaleMessages()
});
