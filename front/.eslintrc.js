module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/recommended',
    'eslint:recommended',
    '@vue/prettier',
    'plugin:import/errors',
    'plugin:import/errors'
  ],
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: '2020'
  },
  settings: {
    'import/resolver': 'webpack'
  },
  globals: {
    COMMIT_HASH: 'readonly',
    VERSION: 'readonly'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'development' ? 'off' : 'error',
    'no-debugger': process.env.NODE_ENV === 'development' ? 'off' : 'error',
    eqeqeq: ['error', 'always'],
    quotes: [
      'error',
      'single',
      { avoidEscape: true, allowTemplateLiterals: false }
    ],
    'no-var': 'error',
    'prefer-const': 'error',
    'no-unused-vars': [
      'error',
      // Ignore no-unused-vars for variables named only with underscores: `_`, `__`, etc.
      {
        varsIgnorePattern: '^_+$',
        argsIgnorePattern: '^_+$',
        caughtErrorsIgnorePattern: '^_+$'
      }
    ],
    'import/no-unresolved': ['off'],
    'import/first': ['error'],
    'import/order': [
      'warn',
      {
        groups: ['builtin', 'external', 'parent', 'sibling', 'index'],
        pathGroups: [
          {
            pattern: '@/**',
            group: 'parent'
          }
        ],
        pathGroupsExcludedImportTypes: ['builtin'],
        'newlines-between': 'always',
        alphabetize: {
          order: 'asc'
        }
      }
    ],
    'import/newline-after-import': ['warn']
  },
  overrides: [
    {
      files: ['**/__tests__/*.{j,t}s?(x)', '**/tests/unit/**/*.{j,t}s?(x)'],
      env: {
        jest: true
      }
    }
  ]
};
