# StreetMuseum

The street deserves its museum.

## Deployment

Docker Compose is the recommended environment to run the components of the project.
Simply edit the [docker-compose.yml](docker/docker-compose.yml) with the proper values.

You can start the docker-compose:

```shell
docker$ docker compose up -d
```

## <a name="run"></a>Run the project

The application is made of 2 bricks: one [Flask](https://flask.palletsprojects.com/) application, the API and one [VueJS](https://vuejs.org/) application, the front.

### The API

You need to have a `virtualenv` and the dependencies install before running the application.

```shell
$ cd api/
$ poetry init
$ poetry shell
(venv) api$ poetry install --with dev
(venv) api$ poetry build -f wheel
```

Create a dot-env file to configure the API (with the right values):

```shell
(venv) api$ cat > .env << _EoF_
FLASK_ENV=development
FLASK_APP=smapi:app
_EoF_
```

You can now start the application:

```shell
(venv) api$ flask run
 * Serving Flask app "smapi:app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: xxx-xxx-xxx
```

#### Configuration

Here are all the configuration parameters available in the dot-env file:

| Name                              | Description | Default                |
| --------------------------------- | ----------- | ---------------------- |
| DATA_PATH                         |             | /opt/streetmuseum/data |
| FLASK_ENV                         |             | development            |
| FLASK_APP                         |             | smapi:app              |
| SECRET_KEY                        |             |                        |
| OPENSEARCH_HOST                   |             | opensearch             |
| OPENSEARCH_PORT                   |             | 9200                   |
| OPENSEARCH_INITIAL_ADMIN_PASSWORD |             | Very_Secure_Passw0rd   |
| AUTH0_DOMAIN                      |             |                        |
| AUTH0_AUDIENCE                    |             |                        |
| AUTH0_CLIENT_ID                   |             |                        |
| AUTH0_CLIENT_SECRET               |             |                        |
| AUTH0_LOGOUT_URL                  |             |                        |
| AUTH0_CALLBACK_URL                |             |                        |
| S3_REGION                         |             |                        |
| S3_ENDPOINT_URL                   |             |                        |
| S3_ACCESS_KEY                     |             |                        |
| S3_SECRET_KEY                     |             |                        |
| S3_BUCKET_ARTWORK                 |             |                        |

### The Front

You need to have a working NodeJS v12.20 installation to run the front application.

(fresh)Install the dependencies:

```shell
$ cd front
front$ npm ci
```

And start the developement server:

```shell
front$ npm run serve
[SNIP]
 DONE  Compiled successfully in 248ms

  App running at:
  - Local:   http://localhost:8080/
  - Network: http://w.x.y.z:8080/
```

## Running Tests

Each application (API vs Front) uses its own testing tools.

### The API

To run tests, you need to have an OpenSearch available. You can start it locally with Docker:

```shell
  $ docker run -d --name smopensearch -p 9200:9200 -p 9600:9600 -e OPENSEARCH_INITIAL_ADMIN_PASSWORD=*** -e "discovery.type=single-node" opensearchproject/opensearch:2
```

You should have the api setup as explained in [Run chapter](#run), meaning the `virtualenv` in place and the application installed with its dependencies in it and the dot-env file in place and edited.

Once you're all set up, you can run the unit tests:

```shell
  $ py.test --cov-report term-missing --cov=smapi tests
```

You can also test the python code against PEP8 by running the linter:

```shell
  $ flake8 smapi/*.py tests/*.py
```

### The Front

You should have the front setup as explained in [Developement chapter](#developement), meaning the application installed with its dependencies.

Once you're all set up, you can run the unit tests:

```shell
  $ npm run test:unit
```

You can also test the JS code against ESlint by running the linter:

```shell
  $ npm run lint
```

## Tech Stack

**Front:**

- [NodeJS](https://nodejs.org/) v12.22.12
- [VueJS](https://vuejs.org/) 2.7.14
- [Bulma](https://bulma.io/) 0.9.2

**API:** [Flask](https://flask.palletsprojects.com/)

**Hosting:** [Gitlab](https://gitlab.com/streetmuseum/street-museum), [Scaleway](https://www.scaleway.com/elements/)

## Authors

- [@chilladx](https://gitlab.com/chilladx)
